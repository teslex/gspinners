# gspinners

**Groovy cli spinners**

![gspinners](/gspinners.gif)


### Usage
```groovy
@GrabResolver(name = 'teslex-repo', root = 'https://gitlab.com/TesLex/repo/raw/master')
@Grab(group = 'tech.teslex.gspinners', module = 'gspinners', version = '1.0.1')
```


```groovy
import tech.teslex.gspinners.set.DotsSpinner

def spinner = new DotsSpinner('Loading..')
spinner.spin()
```

Spinners:
- ArcSpinner
- BallonSpinner
- BallonASpinner
- BounceSpinner
- BouncingBallSpinner
- BouncingBarSpinner
- BoxBounceSpinner
- BoxBounceSpinner
- CircleSpinner
- ClockSpinner
- DotsASpinner
- DotsSpinner
- DotsASpinner
- DotsBSpinner
- DotsCSpinner
- DotsDSpinner
- DotsESpinner
- DotsFSpinner
- DotsGSpinner
- DotsHSpinner
- DotsISpinner
- DotsJSpinner
- DotsKSpinner
- EarthSpinner
- FlipSpinner
- GrowHorizontalSpinner
- GrowVerticalSpinner
- HamburgerSpinner
- LineSpinner
- LineASpinner
- MoonSpinner
- NoiseSpinner
- PipeSpinner
- PointSpinner
- RunnerSpinner
- SimpleDotsScrollingSpinner
- SimpleDotsSpinner
- StarSpinner
- StarASpinner


**Create custom spinner**:
```groovy
import tech.teslex.gspinners.GSpinner

class CustomSpinner extends GSpinner {

	CustomSpinner(String text = '', float interval = 500) {
    	super(text, interval, ["lol", "kek"])
    }
}

def customSpinner = new CustomSpinner('\b..')
customSpinner.spin()
```


Some spinners design from [cli-spinners](https://github.com/sindresorhus/cli-spinners)