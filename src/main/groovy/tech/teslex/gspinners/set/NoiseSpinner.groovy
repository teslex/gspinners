package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class NoiseSpinner extends GSpinner {

	NoiseSpinner(String text = '', float interval = 100) {
		super(text, interval, ["▓", "▒", "░"])
	}
}
