package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class StarASpinner extends GSpinner {

	StarASpinner(String text = '', float interval = 80) {
		super(text, interval, ["+", "x", "*"])
	}
}