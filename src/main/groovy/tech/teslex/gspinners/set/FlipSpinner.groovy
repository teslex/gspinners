package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class FlipSpinner extends GSpinner {

	FlipSpinner(String text = '', float interval = 70) {
		super(text, interval, ["_", "_", "_", "-", "`", "`", "'", "´", "-", "_", "_", "_"])
	}
}
