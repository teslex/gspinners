package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class CircleSpinner extends GSpinner {

	CircleSpinner(String text = '', float interval = 120) {
		super(text, interval, ["◡", "⊙", "◠"])
	}
}
