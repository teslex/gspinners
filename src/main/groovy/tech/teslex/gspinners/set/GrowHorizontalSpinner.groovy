package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class GrowHorizontalSpinner extends GSpinner {

	GrowHorizontalSpinner(String text = '', float interval = 120) {
		super(text, interval, ["▏", "▎", "▍", "▌", "▋", "▊", "▉", "▊", "▋", "▌", "▍", "▎"])
	}
}
