package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class PointSpinner extends GSpinner {

	PointSpinner(String text = '', float interval = 125) {
		super(text, interval, ["∙∙∙", "●∙∙", "∙●∙", "∙∙●", "∙∙∙"])
	}
}

