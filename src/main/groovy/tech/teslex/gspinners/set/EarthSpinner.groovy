package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class EarthSpinner extends GSpinner {

	EarthSpinner(String text = '', float interval = 180) {
		super(text, interval, ["🌍 ", "🌎 ", "🌏 "])
	}
}
