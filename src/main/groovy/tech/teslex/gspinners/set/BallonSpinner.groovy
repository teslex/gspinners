package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class BallonSpinner extends GSpinner {

	BallonSpinner(String text = '', float interval = 140) {
		super(text, interval, [" ", ".", "o", "O", "@", "*", " "])
	}
}

