package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class GrowVerticalSpinner extends GSpinner {

	GrowVerticalSpinner(String text = '', float interval = 120) {
		super(text, interval, ["▁", "▃", "▄", "▅", "▆", "▇", "▆", "▅", "▄", "▃"])
	}
}
