package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class BounceSpinner extends GSpinner {

	BounceSpinner(String text = '', float interval = 120) {
		super(text, interval, ["⠁", "⠂", "⠄", "⠂"])
	}
}
