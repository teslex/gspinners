package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class SimpleDotsScrollingSpinner extends GSpinner {

	SimpleDotsScrollingSpinner(String text = '', float interval = 200) {
		super(text, interval, [".  ", ".. ", "...", " ..", "  .", "   "])
	}
}