package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class LineSpinner extends GSpinner {

	LineSpinner(String text = '', float interval = 130) {
		super(text, interval, ["-", "\\", "|", "/"])
	}
}
