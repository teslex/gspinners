package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class DotsJSpinner extends GSpinner {

	DotsJSpinner(String text = '', float interval = 100) {
		super(text, interval, ["⠁", "⠂", "⠄", "⡀", "⢀", "⠠", "⠐", "⠈"])
	}
}
