package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class BallonASpinner extends GSpinner {

	BallonASpinner(String text = '', float interval = 120) {
		super(text, interval, [".", "o", "O", "°", "O", "o", "."])
	}
}

