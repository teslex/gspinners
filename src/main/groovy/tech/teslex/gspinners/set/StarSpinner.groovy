package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class StarSpinner extends GSpinner {

	StarSpinner(String text = '', float interval = 70) {
		super(text, interval, ["✶", "✸", "✹", "✺", "✹", "✷"])
	}
}