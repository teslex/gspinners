package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class LineASpinner extends GSpinner {

	LineASpinner(String text = '', float interval = 100) {
		super(text, interval, ["⠂", "-", "–", "—", "–", "-"])
	}
}
