package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class PipeSpinner extends GSpinner {

	PipeSpinner(String text = '', float interval = 100) {
		super(text, interval, ["┤", "┘", "┴", "└", "├", "┌", "┬", "┐"])
	}
}
