package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class BoxBounceASpinner extends GSpinner {

	BoxBounceASpinner(String text = '', float interval = 100) {
		super(text, interval, ["▌", "▀", "▐", "▄"])
	}
}
