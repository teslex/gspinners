package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class BoxBounceSpinner extends GSpinner {

	BoxBounceSpinner(String text = '', float interval = 120) {
		super(text, interval, ["▖", "▘", "▝", "▗"])
	}
}
