package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class RunnerSpinner extends GSpinner {

	RunnerSpinner(String text = '', float interval = 140) {
		super(text, interval, ["🚶 ", "🏃 "])
	}
}
