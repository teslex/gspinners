package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class SimpleDotsSpinner extends GSpinner {

	SimpleDotsSpinner(String text = '', float interval = 400) {
		super(text, interval, [".  ", ".. ", "...", "   "])
	}
}