package tech.teslex.gspinners.set

import groovy.transform.CompileStatic
import tech.teslex.gspinners.GSpinner

@CompileStatic
class BouncingBallSpinner extends GSpinner {

	BouncingBallSpinner(String text = '', float interval = 80) {
		super(text, interval, ["( ●    )", "(  ●   )", "(   ●  )", "(    ● )", "(     ●)", "(    ● )", "(   ●  )", "(  ●   )", "( ●    )", "(●     )"])
	}
}
