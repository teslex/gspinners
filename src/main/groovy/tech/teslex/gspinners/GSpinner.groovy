package tech.teslex.gspinners

import groovy.transform.CompileStatic

@CompileStatic
class GSpinner implements Spinner {

	String text
	float interval
	List<String> frames

	int framePos = 0

	GSpinner(String text, float interval, List<String> frames, int framePos = 0) {
		this.text = text
		this.interval = interval
		this.frames = frames
		this.framePos = framePos
	}

	@Override
	void spin() {
		for (int i = 0; i < frames.size(); i++) {
			printNextFrame()
			sleep interval as int
		}
		print '\n'
	}

	void printNextFrame() {
		String s = "${nextFrame()} $text"
		System.out.write "\u001b[${s.length()}D$s".bytes
		System.out.flush()
	}

	@Override
	String nextFrame() {
		String frame = frames[framePos]
		framePos++

		if (framePos >= frames.size())
			framePos = 0

		frame
	}
}
