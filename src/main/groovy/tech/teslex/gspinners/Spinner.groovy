package tech.teslex.gspinners

import groovy.transform.CompileStatic

@CompileStatic
interface Spinner {
	void spin()

	String nextFrame()
}